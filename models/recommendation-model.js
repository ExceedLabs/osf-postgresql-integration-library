const GenericHelper = require('../helpers/generic-helper');
const DatabaseService = require('../services/database-service');
const Tables = require('../const/tables');
const RecommendationConverter = require('./../services/recommendation-converter');

class RecommendationModel {

    static async getRecommendations(coreObj) {
        const { req, MessageService, Converters, intLibraryconf } = GenericHelper.retrieveCoreObjects(coreObj);
        const accountId = req.params.account_id;
        const recomTable = Tables.TABLE_SCHEMA.RECOMMENDATION;
        const getQueryResult = async () => {        
            if(accountId) {
                try {
                    const query = `SELECT ${recomTable.rating.name}, 
                                        ${recomTable.product_id.name} 
                                FROM ${Tables.TABLE_NAMES.RECOMMENDATION} 
                                WHERE ${Tables.TABLE_SCHEMA.RECOMMENDATION.account_id.name} = '${accountId}'`;
                    const res =  await DatabaseService.executeQuery(intLibraryconf.credentials, query);
                    if (res.rows.length == 0) {
                        return MessageService.getSuccess([], 200, 'Not recommendaitons for the accountId informed');
                    }
                    return MessageService.getSuccess(res.rows, 200, 'OK');
                } catch (error) {
                    return MessageService.getError('Error executing DB query', 500, 'An erro ocurred on retrieveing values form the DB');
                }
            } else {
                return MessageService.getError('Invalid client accountId', 400, 'The client accountId is not valid');
            }
        }
        const result = await getQueryResult();

        /* Get the result from DB and converter the fields for the Core pattern of fields names */
        if (result.status == 200){
            const { map, data } = RecommendationConverter.getDataMapToCore(coreObj, result.data);
            const dataReplaced = Converters.replaceFields(data, map);
            return MessageService.getSuccess(dataReplaced, result.status, result.message);
        } else {
            return result;
        }       
    }
}

module.exports = RecommendationModel;