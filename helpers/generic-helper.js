class GenericHelper {
    static retrieveCoreObjects (coreOjb) {
        const { req } = coreOjb;
        const { MessageService, Converters } = coreOjb.privateLibraries;
        const intLibraryconf = coreOjb.infoOrganization.integrationLibs.find( elem => elem.id == 'osf-postgresql-integration-library');
        return { req, MessageService, Converters, intLibraryconf }
    }
}

module.exports = GenericHelper;