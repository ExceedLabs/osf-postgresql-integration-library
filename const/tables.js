module.exports = {
    TABLE_NAMES: {
        RECOMMENDATION: 'salesforce.recommendations',
        ABANDONEDCART: 'salesforce.abandonedcart',
        ABANDONEDCARTITEMS: 'salesforce.abandonedcartitems',
        LASTVIEWED: 'salesforce.igoproductviews'
    },
    TABLE_SCHEMA: {
        RECOMMENDATION: {
            rating: {
                name: 'rating',
                type: 'float'
            },
            account_id: {
                name: 'account_id',
                type: 'string'
            },
            product_id: {
                name: 'product_id',
                type: 'string'
            }
        },
        ABANDONEDCART: {
            name: {
                name: 'name',
                type: 'string'
            },
            abandoned_cart_id: {
                name: 'abandoned_cart_id',
                type: 'string'
            },
            account_id: {
                name: 'name',
                type: 'string'
            },
            is_latest_abandoned_cart: {
                name: 'is_latest_abandoned_cart',
                type: 'boolean'
            },
            is_recovered_basket: {
                name: 'is_recovered_basket',
                type: 'boolean'
            },
            last_modified: {
                name: 'last_modified',
                type: 'datetime'
            },
            creation_date: {
                name: 'creation_date',
                type: 'datetime'
            },
            recovered_by_order_number: {
                name: 'recovered_by_order_number',
                type: 'datetime'
            },
            basket_id: {
                name: 'basket_id',
                type: 'string'
            },
            sfcc_site_id: {
                name: 'sfcc_site_id',
                type: 'string'
            },
            currency_code: {
                name: 'currency_code',
                type: 'string'
            },
            items_number: {
                name: 'items_number',
                type: 'string'
            },
            items_xml: {
                name: 'items_xml',
                type: 'string'
            },
            total: {
                name: 'total',
                type: 'double'
            },
            recover_url: {
                name: 'recover_url',
                type: 'string'
            },
            source_integration: {
                name: 'source_integration',
                type: 'string'
            }
        },
        ABANDONEDCARTITEMS: {
            abandoned_cart_id: {
                name: 'abandoned_cart_id',
                type: 'string'
            }, 
            pricebook_id: {
                name: 'pricebook_id',
                type: 'unit_price'
            },
            unit_price: {
                name: 'unit_price',
                type: 'double'
            },
            quantity: {
                name: 'quantity',
                type: 'double'
            },
            abandoned_cart_item_id: {
                name: 'abandoned_cart_item_id',
                type: 'string'
            },
            added_date: {
                name: 'added_date',
                type: 'Datetime'
            },
            variation_attribute_size: {
                name: 'variatiton_attribute_size',
                type: 'string'
            },
            variatiton_attribute_color: {
                name: 'variation_attribute_color',
                type: 'string'
            }
        },
        LASTVIEWED: {
            id: {
                name: 'id',
                type: 'string'
            },
            product_sku: {
                name: 'product_sku',
                type: 'string'
            },
            account_id: {
                name: 'account_id',
                type: 'string'
            },
            session_id: {
                name: 'session_id',
                type: 'string'
            },
            search: {
                name: 'search',
                type: 'string'
            },
            viewed_time: {
                name: 'viewed_time',
                type: 'datetime'
            }
        }
    }
}