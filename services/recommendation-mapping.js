const FieldsConstant = require('./../const/recommendation-constant');
 
const FIELDS = FieldsConstant.fields;

class RecommendationMapping {
    static getCoreFields(fieldsCore) {
        return {
            [fieldsCore.product_id]: FIELDS.product_id,
            [fieldsCore.rating]: FIELDS.rating
        };
    }
}

module.exports = RecommendationMapping;