const { Client } = require('pg');


/**
 * Instantiate the client object with the credentials
 * @param {JSON} config 
 * credentials example:
 * {"user":"userExample", "host":"hostExample", "password":"passwordExample", "database":"databaseExample", "port":"portExample", "ssl":true}
 */
const connect = async (config) => {
    try {
        const client = new Client(config);
        await client.connect();
        return client;
    } catch (error) {
        throw error;
    }
};

class DatabaseService {

    /**
     * Execute the query on the database assigned on the credentials
     * @param {JSON} credentials 
     * credentials example:
     * {"user":"userExample", "host":"hostExample", "password":"passwordExample", "database":"databaseExample", "port":"portExample", "ssl":true}
     * @param {String} queryString 
     */
    async executeQuery(credentials, queryString) {
        const client = await connect(credentials);
        try {
            const res = await client.query(queryString);
            return res;
        } catch (error) {
            throw error;
        } finally {
            client.end();
        }        
    };
}

module.exports = new DatabaseService();