const RecommendationMapping = require('./recommendation-mapping');

class RecommendationConverter {
    static getDataMapToCore(obj, recommendationData) {
        const { fieldsCore } = obj.privateLibraries.Converters;
        const { libObjId } = obj;

        const map = {
            list: libObjId,
            item: {
                ...RecommendationMapping.getCoreFields(fieldsCore),
            },
        };

        const data = {
            [libObjId]: recommendationData,
        };
        
        return {
            map,
            data,
        };
    }
}

module.exports = RecommendationConverter;